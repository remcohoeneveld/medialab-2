<?php ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Medialab 2 | Design for one</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="../../lib/css/style.css">
</head>
<body class="main">

<div class="header">
        <div class="col-md-4">
            <a href="../inside.php">
                <img class="backicon" src="../../lib/img/go-back-left-arrow.png">
            </a>
        </div>
        <div class="col-md-4 ">
            Tv kijken
        </div>
        <div class="col-md-4">
            <a href="../../Home.php">
                <img class="homeicon" src="../../lib/img/home-button.png">
            </a>
        </div>
</div>

<div class="container whitebackground">
    <div class="col-sm-3 padding5">
        <div class="activityAction">
            <div id="div1">
                <img class="activityIcon" src="../../lib/img/stop-sign-variant.png">
                Stop activiteit
            </div>
        </div>
    </div>
    <div class="col-sm-3 padding5">
        <div class="activityAction">
            <div id="div2">
                <img class="activityIcon" src="../../lib/img/remote-control.png">
               Ik wil zappen
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="activityAction">
            <div id="div3">
                <img class="activityIcon" src="../../lib/img/volume-up-interface-symbol.png">
                Geluid aanpassen
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="activityAction">
            <div id="div4">
                <img class="activityIcon" src="../../lib/img/shuffle.png">
                Andere activiteit
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="activityAction">
            <div id="div5">
                <img class="activityIcon" src="../../lib/img/min.png">
                Geluid zachter zetten
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="activityAction">
            <div id="div6">
                <img class="activityIcon" src="../../lib/img/plus.png">
                Geluid harder zetten
            </div>
        </div>
    </div>
    <!--    <div class="col-sm-3">-->
    <!--        <div class="activityAction">-->
    <!--            <div><a href="#">Ik wil wat eten</a></div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--    <div class="col-sm-3">-->
    <!--        <div class="activityAction">-->
    <!--            <div><a href="#">Ik wil wat drinken</a></div>-->
    <!--        </div>-->
    <!--    </div>-->

</div>
<script type="text/javascript" src="../../lib/js/responsive_voice.js"></script>
<script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
<script src="../../lib/js/selectActivity.js"></script>
</body>
</html>