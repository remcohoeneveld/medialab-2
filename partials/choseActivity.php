<?php ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Medialab 2 | Design for one</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="../lib/css/style.css">
</head>
<body class="main">

<div class="header">
        <div class="col-md-4">
            <a href="inside.php">
                <img class="backicon" src="../lib/img/go-back-left-arrow.png">
            </a>
        </div>
        <div class="col-md-4 ">
            Activiteit kiezen
        </div>
        <div class="col-md-4">
            <a href="../Home.php">
                <img class="homeicon" src="../lib/img/home-button.png">
            </a>
        </div>
</div>

<div class="container whitebackground">
    <div class="col-sm-6">
        <div class="actionChose">
            <div><a href="inside.php">Binnen activiteiten</a></div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="actionCreate">
            <div><a href="">Buiten activiteiten</a></div>
        </div>
    </div>
</div>
</body>
</html>