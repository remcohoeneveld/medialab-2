<?php ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Medialab 2 | Design for one</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="../lib/css/style.css">
</head>
<body class="main">

<div class="header">
        <div class="col-md-4">
            <a href="choseActivity.php">
                <img class="backicon" src="../lib/img/go-back-left-arrow.png">
            </a>
        </div>
        <div class="col-md-4 ">
            Binnen activiteiten
        </div>
        <div class="col-md-4">
            <a href="../Home.php">
                <img class="homeicon" src="../lib/img/home-button.png">
            </a>
        </div>
</div>

<div class="container whitebackground">
    <div class="col-sm-3 padding5">
        <div class="activityChose">
            <div>
                <a href="#">Eten</a>
            </div>
        </div>
    </div>
    <div class="col-sm-3 padding5">
        <div class="activityChose">
            <div><a href="#">Drinken</a></div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="activityChose">
            <div><a href="#">Lezen</a></div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="activityChose">
            <div><a href="activities/tvkijken.php">Tv Kijken</a></div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="activityChose">
            <div><a href="#">Film Kijken</a></div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="activityChose">
            <div><a href="#">Muziek Luisteren</a></div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="activityChose">
            <div><a href="#">Youtube kijken</a></div>
        </div>
    </div>

</div>
</body>
</html>