<?php ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Medialab 2 | Design for one</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="lib/css/style.css">
</head>
<body class="main">

<div class="header">
        <div class="col-md-12 ">
            Home
        </div>
</div>
<div class="container whitebackground">
    <div class="col-sm-4 padding5">
        <div class="actionChose">
            <div id="div1"><a href="partials/choseActivity.php">Activiteit kiezen</a></div>
        </div>
    </div>
    <div class="col-sm-4 padding5">
        <div class="actionCreate">
            <div id="div2"><a href="">Activiteit aanmaken/wijzigen</a></div>
        </div>
    </div>
    <div class="col-sm-4 padding5">
        <div class="systemSetting">
            <div id="div3"><a href="">Systeem Instellingen</a></div>
        </div>
    </div>
</div>
</body>

</html>