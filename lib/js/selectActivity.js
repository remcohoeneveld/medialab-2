/**
 * Created by remcohoeneveld on 17/05/2017.
 */

$(function () {

    var counter = 0,
        divs = $('#div1, #div2, #div3, #div4, #div5, #div6');

    function showDiv () {
        divs.removeClass('activeAct')
            .filter(function (index) { return index == counter % 6; }) // figure out correct div to show
            .addClass('activeAct'); // and show it

        counter++;
    }; // function to loop through divs and show correct div

    function checkClass(){
        var activeText = $('div[class*="activeAct"]').text();
        console.log(activeText);
    }

    $(document).keyup(function (evt) {
        if (evt.keyCode == 13) {

        }
        // what happens when key is down
    }).keydown(function (evt) {
        if (evt.keyCode == 13) {
            responsiveVoice.speak($('div[class*="activeAct"]').text(), "Dutch Female");
        }
    });

    checkClass();
    showDiv(); // show first div

    setInterval(function () {
        showDiv(); // show next div
        checkClass();
    }, 5 * 1000); // do this every 10 seconds

});

